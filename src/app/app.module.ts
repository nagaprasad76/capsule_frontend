import { AppComponent } from './app.component';
import { TaskFilterPipe } from './pipe/filter.pipe';
import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {DatePipe} from '@angular/common';
import { AddTaskComponent } from './ui/addtask/addtask.component';
import { ViewTaskComponent } from './ui/viewtask/viewtask.component';
import { UpdateTaskComponent } from './ui/updatetask/updatetask.component';
import { SharedService } from './service/shared.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

var rulesParent =[
    {path:"addtask", component:AddTaskComponent},
    {path:"", component:ViewTaskComponent},
    {path:"viewtask", component:ViewTaskComponent},
    {path:"updatetask/:tid", component:UpdateTaskComponent}
]

@NgModule({
    imports: [BrowserModule,BrowserAnimationsModule,FormsModule,ReactiveFormsModule,BsDatepickerModule.forRoot(),RouterModule,HttpClientModule,RouterModule.forRoot(rulesParent)],
    exports: [],
    declarations: [AppComponent,AddTaskComponent,ViewTaskComponent,UpdateTaskComponent,TaskFilterPipe],
    providers: [DatePipe,SharedService],
    bootstrap:[AppComponent]
})


export class AppModule { }
