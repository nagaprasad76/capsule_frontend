import { Task } from 'src/app/model/task.model';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'taskFilter'
})

export class TaskFilterPipe implements PipeTransform {
    transform(values: Task[], taskName: string, parentTaskName: string,
        priorityFrom: number, priorityTo: number, startDate: Date, endDate: Date): any {

        if (values && values.length) {
            return values.filter(item => {
                if (taskName && !item.TaskName.toLocaleLowerCase().startsWith(taskName.toLocaleLowerCase())) {
                    return false;
                }
                if (parentTaskName) {
                    if (!item.ParentTaskName)
                        return false;
                    if (item.ParentTaskName && item.ParentTaskName.toLocaleLowerCase().indexOf(parentTaskName.toLocaleLowerCase()) === -1)
                        return false;
                }
                if (priorityFrom && item.Priority < priorityFrom) {
                    return false;
                }

                if (priorityTo && item.Priority > priorityTo) {
                    return false;
                }
                if (startDate && new Date(item.StartDate).getTime() < startDate.getTime()) {
                    return false;
                }
                if (endDate && new Date(item.EndDate).getTime() > endDate.getTime()) {
                    return false;
                }
                return true;
            })
        }
        else {
            return values;
        }
    }
}