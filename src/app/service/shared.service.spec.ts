import { HttpClientModule } from '@angular/common/http';
import { Task } from 'src/app/model/task.model';
import { TestBed } from '@angular/core/testing';
import { SharedService } from './shared.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('SharedService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports:[HttpClientTestingModule,HttpClientModule],
      providers:[SharedService]
  }));

  it('should be created', () => {
    const service: SharedService = TestBed.get(SharedService);
    expect(service).toBeTruthy();
  });
  
  it('Should return records',() => {
      const service : SharedService = TestBed.get(SharedService);
      const list = service.GetAll();
      expect(list).toBeDefined();
  });

});
