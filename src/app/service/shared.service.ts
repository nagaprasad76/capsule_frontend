import { Task } from './../model/task.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';



@Injectable()
export class SharedService {
    baseUrl: string = 'http://localhost/TaskManager/api/'
    constructor(private hc: HttpClient) { }

    //get all tasks
    GetAll(): Observable<any> {
        return this.hc.get<Task[]>(this.baseUrl + "GetAll")
            .pipe(
                tap(tasks => console.log('Fetch Tasks')),
                catchError(this.handleError('GetAll', []))
            );
    }

    //get task by id
    Get(id: number): Observable<any> {
        return this.hc.get<Task>(this.baseUrl + "Get/" + id)
            .pipe(
                tap(_ => console.log('Fetched Task id=' + id)),
                catchError(this.handleError<Task>('Get id=' + id))
            );
    }

    //add new task
    Add(item: Task): Observable<any> {
        return this.hc.post(this.baseUrl + "Add", item)
            .pipe(
                tap((item: Task) => console.log('Added task w/ id=' + item.TaskID)),
                catchError(this.handleError<Task>('Add'))
            );
    }

    //Update existing task
    Update(item: Task): Observable<any> {
        return this.hc.put(this.baseUrl + "Update", item)
            .pipe(
                tap(_ => console.log('Updated task id=' + item.TaskID)),
                catchError(this.handleError<any>('Update'))
            );
    }

    //End the task 
    End(item: Task): Observable<any> {
        return this.hc.put(this.baseUrl + "End", item)
            .pipe(
                tap(_ => console.log('Status ended task id=' + item.TaskID)),
                catchError(this.handleError<any>('End'))
            );
    }
    //Delete existing task
    Delete(id: number): Observable<any> {
        return this.hc.delete(this.baseUrl + "Delete/" + id)
            .pipe(
                tap(_ => console.log('Deleted Task id=' + id)),
                catchError(this.handleError<Task>('Delete'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error); // log to console instead
            return of(result as T);
        };
    }
}