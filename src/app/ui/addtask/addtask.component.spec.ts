import { FormsModule } from '@angular/forms';
import { AddTaskComponent } from './addtask.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedService } from './../../service/shared.service';
import { HttpClient,HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {RouterModule} from '@angular/router';
import { DatePipe } from '@angular/common';

describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskComponent ],
      imports: [ FormsModule,RouterTestingModule, RouterModule],
      providers:[SharedService,HttpClient,HttpHandler,DatePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
