import { SharedService } from './../../service/shared.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Task } from 'src/app/model/task.model';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'addtask',
    templateUrl: 'addtask.component.html'
})

export class AddTaskComponent implements OnInit {
    task = new Task();
    tasks: Task[];
    @ViewChild("newForm") newForm: NgForm;

    constructor(private taskService: SharedService, private router: Router, private datePipe: DatePipe) {

        this.taskService.GetAll().subscribe((response) => {
            this.tasks = <Task[]>(response);
        })
    }

    ngOnInit() {
        this.newForm.resetForm();
    }

    addtask() {
        if (this.newForm.valid) {
            this.task.StartDate = new Date(this.datePipe.transform(this.task.StartDate, "yyyy-MM-dd"));
            this.task.EndDate = new Date(this.datePipe.transform(this.task.EndDate, "yyyy-MM-dd"));
            if (!this.task.Priority) {
                this.task.Priority = 0;
            }
            this.taskService.Add(this.task).subscribe((response) => {
                alert("Task added successfully");
                this.router.navigate(['/viewtask']);
            })
        }
    }
}