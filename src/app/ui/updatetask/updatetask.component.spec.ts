import { Task } from './../../model/task.model';
import { UpdateTaskComponent } from './updatetask.component';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedService } from './../../service/shared.service';
import { HttpClient,HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {RouterModule} from '@angular/router';
import { DatePipe } from '@angular/common';


describe('UpdateTaskComponent', () => {
  let component: UpdateTaskComponent;
  let fixture: ComponentFixture<UpdateTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTaskComponent ],
      imports: [ FormsModule,RouterTestingModule, RouterModule],
      providers:[SharedService,HttpClient,HttpHandler,DatePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //it('should create', () => {
   // expect(component).toBeTruthy();
  //});
});
