import { SharedService } from './../../service/shared.service';
import { Component, OnInit, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Task } from 'src/app/model/task.model';

@Component({
    selector: 'updatetask',
    templateUrl: 'updatetask.component.html'
})

export class UpdateTaskComponent implements OnInit {
    task = new Task();
    parentTasks: Task[];
    constructor(private taskService: SharedService, private router: Router, private ac: ActivatedRoute, private datePipe: DatePipe) {
        var taskid = this.ac.snapshot.params["tid"];
        this.taskService.Get(taskid).subscribe((response) => {
            this.task = <Task>response;
            this.task.StartDate = new Date(this.datePipe.transform(this.task.StartDate, "MM/dd/yyyy"));
            this.task.EndDate = new Date(this.datePipe.transform(this.task.EndDate, "MM/dd/yyyy"));
        })
        this.taskService.GetAll().subscribe((response) => {
            this.parentTasks = <Task[]>(response);
            this.parentTasks = this.parentTasks.filter( item => item.TaskID !== taskid);
        })
    }

    ngOnInit() { }
    update() {
        console.log(this.task);
        this.taskService.Update(this.task).subscribe((response) => {
            alert("Record Updated Successfully");
            this.router.navigate(['/viewtask']);
        })
    }
    cancel() {
        this.router.navigate(['/viewtask']);
    }
}