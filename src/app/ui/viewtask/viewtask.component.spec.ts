import { TaskFilterPipe } from '../../pipe/filter.pipe';
import { ViewTaskComponent } from './viewtask.component';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedService } from './../../service/shared.service';
import { HttpClient,HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {RouterModule} from '@angular/router';
import { DatePipe } from '@angular/common';

describe('ViewTaskComponent', () => {
  let component: ViewTaskComponent;
  let fixture: ComponentFixture<ViewTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTaskComponent,TaskFilterPipe ],
      imports: [ FormsModule,RouterTestingModule, RouterModule],
      providers:[SharedService,HttpClient,HttpHandler,DatePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
