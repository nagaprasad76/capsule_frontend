import { SharedService } from './../../service/shared.service';
import { Task } from './../../model/task.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'viewtask',
  templateUrl: 'viewtask.component.html'
})

export class ViewTaskComponent implements OnInit {
  tasks: Task[];
  search:any={};
  task = new Task();
  constructor(private taskService: SharedService, private router: Router) {
    this.taskService.GetAll().subscribe((response) => {
      this.tasks = <Task[]>(response);
    })
  }

  ngOnInit() { }

  updatetask(id) {
    this.router.navigate(['/updatetask', id]);
  }

  endtask(task) {
    this.taskService.End(task)
      .subscribe(resp => {
        this.tasks = resp;
      });

  }

  deletetask(id) {
    this.taskService.Delete(id).subscribe((response) => {
      alert("Record deleted Successfully");
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
        this.router.navigate(["/viewtask"]));
    }, (error) => {
      alert(error.error);
    })
  }
}